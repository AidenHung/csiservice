/**
* PHP Email Form Validation - v3.1
* URL: https://bootstrapmade.com/php-email-form/
* Author: BootstrapMade.com
*/
(function () {
  "use strict";

  let forms = document.querySelectorAll('.php-email-form');

  forms.forEach( function(e) {
    e.addEventListener('submit', function(event) {
      event.preventDefault();

      let thisForm = this;

      let action = thisForm.getAttribute('action');
      let recaptcha = thisForm.getAttribute('data-recaptcha-site-key');
      
      if( ! action ) {
        displayError(thisForm, 'The form action property is not set!')
        return;
      }
      let strdomain = "@ch-si.com.tw";
      if((! checkDomain(thisForm.querySelector('#mailFrom').value,strdomain)) || 
          (! checkDomain(thisForm.querySelector('#applyAccount').value,strdomain))) {
        displayError(thisForm, 'The email domain must be @ch-si.com.tw')
        return;
      }

      thisForm.querySelector('.loading').classList.add('d-block');
      thisForm.querySelector('.error-message').classList.remove('d-block');
      thisForm.querySelector('.sent-message').classList.remove('d-block');

      let formData = new FormData( thisForm );

      if ( recaptcha ) {
        if(typeof grecaptcha !== "undefined" ) {
          grecaptcha.ready(function() {
            try {
              grecaptcha.execute(recaptcha, {action: 'php_email_form_submit'})
              .then(token => {
                formData.set('recaptcha-response', token);
                php_email_form_submit(thisForm, action, formData);
              })
            } catch(error) {
              displayError(thisForm, error)
            }
          });
        } else {
          displayError(thisForm, 'The reCaptcha javascript API url is not loaded!')
        }
      } else {
        php_email_form_submit(thisForm, action, formData);
      }
    });
  });

  function php_email_form_submit(thisForm, action, formData) {
    $.ajax({
      url: 'https://pubmfamicroserviceapi.azurewebsites.net/api/mfa/Register',
      type: 'POST',   
      contentType : 'application/json; charset=urf-8',  
      dataType: 'json',  
      data: JSON.stringify({
        sourceCode: thisForm.querySelector('#sourceCode').value,
        brandName: thisForm.querySelector('#brandName').value, 
        mailFrom: thisForm.querySelector('#mailFrom').value,
        mailName: thisForm.querySelector('#mailName').value,
        mailPassword: thisForm.querySelector('#mailPassword').value,
        applyAccount: thisForm.querySelector('#applyAccount').value
      }),        
      error: function (xhr) {
          displayError(thisForm, "Ajax request 發生錯誤");
      },
      success: function () {
        console.log("success")
        thisForm.querySelector('.loading').classList.remove('d-block');
        thisForm.querySelector('.sent-message').classList.add('d-block');
        thisForm.reset(); 
      }
    })
  }

  function displayError(thisForm, error) {
    thisForm.querySelector('.loading').classList.remove('d-block');
    thisForm.querySelector('.error-message').innerHTML = error;
    thisForm.querySelector('.error-message').classList.add('d-block');
  }

  function checkDomain(userInput, domain) {
    // Check the substring starting at the @ against the domain
    return (userInput.substring(userInput.indexOf('@')) === domain);
  }

})();
